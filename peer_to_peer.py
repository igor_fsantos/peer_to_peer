import socket
import threading
import select
import time
import sys
import os
import ast
from random import shuffle

# Caminho para a pasta onde os arquivos sao compartilhados
PATH = ''
# Caminho secundario para facilitar testes feitos de forma local
PATH2 = ''
# Nome do arquivo
FILE_NAME = "file_name.file_format"
# Tamanho maximo dos pacotes trocados entre os pares
BUFFER_SIZE = 8192
# Tamanho maximo das mensagens contidas nos pacotes (excluindo o cabecalho)
MESSAGE_SIZE = 200
# Estrutura dos pacotes
'''
mt = message_type (tipo da mensagem, usada para requisicao, resposta e erro)
fn = file_name (nome do arquivo)
tp = total_file_parts (numero total de partes que o arquivo possui)
fp = file_part (inteiro representando uma parte do arquivo)
fd = file_data (parte dos pacotes de dados que constituem o arquivo)
rp = requested_parts (lista que contem um numero limitado de partes que um cliente esta requisitando)
'''
PACKAGE_INFO = '{"mt":"", "fn":"", "tp":""}'
PACKAGE_SERVER = '{"mt":"", "fp":"", "fd":""}'
PACKAGE_CLIENT = '{"mt":"", "fn":"", "rp":""}'
# Numero maximo de erros tolerados na comunicacao, incluindo timeouts
MAX_ERRORS = 3
# Numero maximo de pacotes que o cliente requisita
MAX_PACK_REQ = 20
# Representacao de nenhum pacote requisitado
PACK_NONE = -1
# Tipos de mensagens
MSG_REQUEST = 0
MSG_REPLY = 1
MSG_REQUEST_INFO = 2
MSG_PACKAGE_NOT_FOUND = 3
MSG_FILE_NOT_FOUND = 4
# Lista de IPs e portas disponiveis para o cliente se conectar
IP_LIST = ["127.0.0.1/1776"]

def strip_file_name(file_name):
    name = file_name.partition('.')[0]
    return name

def get_file_folder(name, file_path):
    file_folder = file_path + '\\' + name + '_file_info'
    return file_folder

def only_empty_spaces(data):
    for c in data:
        if ord(c) != 0:
            return False
    return True

def main():
    class Server(threading.Thread):
        def __init__(self):
            threading.Thread.__init__(self)
            self.conn = None
            self.addr = None
            self.running = True
            self.server_socket = None
            # Lista que gerencia os descritores de socket
            self.CONNECTION_LIST = []
            self.last_requested_file = str()

        def kill(self):
            self.running = False

        def create_file_info(self, package_serialized, file_path, file_name):
            name = strip_file_name(file_name)
            file_folder = get_file_folder(name, file_path)
            if not os.path.exists(file_folder + '\\file_info.txt'):
                if not os.path.isdir(file_folder):
                    os.makedirs(file_folder)
                file_size = os.path.getsize(file_path + '\\' + file_name)
                if(file_size % MESSAGE_SIZE == 0):
                    total_file_parts = file_size / MESSAGE_SIZE
                else:
                    total_file_parts = (file_size / MESSAGE_SIZE) + 1
                info_parsed = ast.literal_eval(package_serialized)
                info_parsed["mt"] = MSG_REPLY
                info_parsed["fn"] = file_name
                info_parsed["tp"] = total_file_parts
                info_serialized = str(info_parsed)
                f = open(file_folder + '\\file_info.txt', 'wb')
                f.write(info_serialized)
                f.close()

        def send_file_info(self, package_parsed, file_path, client_socket):
            file_name = package_parsed["fn"]
            name = strip_file_name(file_name)
            file_folder = get_file_folder(name, file_path)
            if os.path.exists(file_folder + '\\file_info.txt'):
                f = open(file_folder + '\\file_info.txt', "rb")
                file_info_serialized = f.read(BUFFER_SIZE)
                f.close()
            else:
                package_parsed["mt"] = MSG_FILE_NOT_FOUND
                file_info_serialized = str(package_parsed)
            # Enviando o pacote resposta ao cliente
            client_socket.send(file_info_serialized)

        def send_file_part(self, package_parsed, server_package, client_socket, file):
            reply_parsed = ast.literal_eval(server_package)
            requested_parts = package_parsed["rp"]
            for i in requested_parts:
                file.seek(i * MESSAGE_SIZE)
                file_data = file.read(MESSAGE_SIZE)
                if file_data and not only_empty_spaces(file_data):
                    reply_parsed["fp"] = i
                    reply_parsed["fd"] = file_data
                    reply_parsed["mt"] = MSG_REPLY
                    break
            if reply_parsed["mt"] != MSG_REPLY:
                reply_parsed["mt"] = MSG_PACKAGE_NOT_FOUND
            reply_serialized = str(reply_parsed)
            client_socket.send(reply_serialized)

        def run(self):
            HOST = ''
            PORT = 1776
            self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            try:
                self.server_socket.bind((HOST, PORT))
            except:
                print "Erro ao definir o servidor..."
                sys.exit()
            self.server_socket.listen(10)
            # Adiciona o socket do servidor na lista de conexoes
            self.CONNECTION_LIST.append(self.server_socket)
            # Criando as informacoes dos arquivos compartilhados caso elas nao existam
            self.create_file_info(PACKAGE_INFO, PATH, FILE_NAME)
            print "O servidor esta executando..."
            # Arquivo a ser lido
            file = None
            while self.running:
                # Obtem lista de sockets que estao prontos para serem lidos por meio do select
                read_sockets, write_sockets, error_sockets = select.select(self.CONNECTION_LIST, [], [])
                for sock in read_sockets:
                    # Nova conexao
                    if sock == self.server_socket:
                        # Tratamento de uma nova conexao
                        self.conn, self.addr = self.server_socket.accept()
                        self.CONNECTION_LIST.append(self.conn)
                        print "Cliente (%s, %s) se conectou." % (self.conn, self.addr)
                    # Obtendo mensagem de um cliente
                    else:
                        try:
                            request_serialized = sock.recv(BUFFER_SIZE)
                            if request_serialized:
                                request_parsed = ast.literal_eval(request_serialized)
                                if request_parsed["mt"] == MSG_REQUEST_INFO:
                                    self.send_file_info(request_parsed, PATH, sock)
                                elif request_parsed["mt"] == MSG_REQUEST:
                                    if request_parsed["fn"] != self.last_requested_file:
                                        self.last_requested_file = request_parsed["fn"]
                                        if file:
                                            file.close()
                                        file = open(PATH + '\\' + self.last_requested_file, 'rb')
                                    file.seek(0)
                                    self.send_file_part(request_parsed, PACKAGE_SERVER, sock, file)
                        except:
                            print "Cliente (%s, %s) esta offline." % (sock.getpeername())
                            sock.close()
                            self.CONNECTION_LIST.remove(sock)
                time.sleep(0)
            if file:
                file.close()
            self.server_socket.close()

    class Client(threading.Thread):
        def __init__(self):
            threading.Thread.__init__(self)
            self.sock = None
            self.running = True
            self.client_socket = None
            self.num_errors = 0
            self.total_file_parts = 0
            self.count_file_parts = 0
            self.request_list = []
            self.last_part_requested = PACK_NONE

        def kill(self):
            self.running = False

        def save_package(self, file_name, file_folder, package_parsed):
            #package_serialized = str(package_parsed)
            package_serialized = str(package_parsed)
            # Salvando o arquivo em disco
            f = open(file_folder + '\\' + file_name, 'wb')
            f.write(package_serialized)
            f.close()

        def request_file_info(self, file_name, file_folder, package_info):
            # Informando o arquivo e o tipo de requisicao para enviar a mensagem ao servidor
            request_parsed = ast.literal_eval(package_info)
            request_parsed["mt"] = MSG_REQUEST_INFO
            request_parsed["fn"] = file_name
            # Serializando o pacote de requisicao
            request_serialized = str(request_parsed)
            while True:
                if self.num_errors >= MAX_ERRORS:
                    if not self.connect_to_server(IP_LIST):
                        sys.exit()
                try:
                    self.client_socket.send(request_serialized)
                    # Recebendo a resposta do servidor
                    file_info_serialized = self.client_socket.recv(BUFFER_SIZE)
                    if file_info_serialized:
                        file_info_parsed = ast.literal_eval(file_info_serialized)
                        if file_info_parsed["mt"] == MSG_FILE_NOT_FOUND:
                            print "O servidor nao possui o arquivo requerido."
                            if not self.connect_to_server(IP_LIST):
                                sys.exit()
                        else:
                            self.total_file_parts = file_info_parsed["tp"]
                            self.save_package("file_info.txt", file_folder, file_info_parsed)
                            self.num_errors = 0
                            break
                except socket.error:
                    print "Erro ao tentar estabelecer comunicacao com o servidor."
                    self.num_errors += 1
                    continue

        def load_file_info(self, file_folder):
            with open(file_folder + '\\file_info.txt', "rb") as file_info:
                file_info_serialized = file_info.read(BUFFER_SIZE)
            file_info.close()
            file_info_parsed = ast.literal_eval(file_info_serialized)
            self.total_file_parts = file_info_parsed["tp"]

        def verify_file_parts(self, start_int, file):
            if start_int == 0:
                self.count_file_parts = 0
            for j in range(start_int, self.total_file_parts):
                file.seek(j * MESSAGE_SIZE)
                data = file.read(MESSAGE_SIZE)
                if data and not only_empty_spaces(data):
                    self.count_file_parts += 1
                else:
                    self.request_list.append(j)
                    self.last_part_requested = j
                    if len(self.request_list) == MAX_PACK_REQ:
                        break
            shuffle(self.request_list)

        def file_check(self, file_name, file_path, package_info, file):
            name = strip_file_name(file_name)
            file_folder = get_file_folder(name, file_path)
            # Verificando se o pacote de informacao do arquivo ja esta criado
            if os.path.exists(file_folder + '\\file_info.txt'):
                # Carregando as informacoes do arquivo
                self.load_file_info(file_folder)
            else:
                # Verificando se a pasta que ira conter as informacoes do arquivo ja existe
                if not os.path.isdir(file_folder):
                    # Criando a pasta caso ela nao exista
                    os.makedirs(file_folder)
                self.request_file_info(file_name, file_folder, package_info)
            self.verify_file_parts(0, file)

        def connect_to_server(self, ip_list):
            if self.client_socket:
                self.client_socket.close()
            self.client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.client_socket.settimeout(5)
            print "Tentando se conectar com um servidor..."
            while len(ip_list) > 0:
                host = ip_list[0]
                ip, port = host.split("/")
                port = int(port)
                try:
                    self.client_socket.connect((ip, port))
                    ip_list.remove(host)
                    self.num_errors = 0
                    print "Conexao efetuada com sucesso com o servidor."
                    return True
                except:
                    ip_list.remove(host)
            print "Nao ha servidores disponiveis para fazer a conexao"
            return False

        def run(self):
            #self.client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            #self.client_socket.settimeout(5)
            if not self.connect_to_server(IP_LIST):
                sys.exit()
            #Arquivo a ser construido
            if not os.path.exists(PATH2 + '\\' + FILE_NAME):
                file = open(PATH2 + '\\' + FILE_NAME, 'wb')
                file.close()
            file = open(PATH2 + '\\' + FILE_NAME, 'r+b')
            # Definindo as configuracoes para o recebimento do arquivo
            self.file_check(FILE_NAME, PATH2, PACKAGE_INFO, file)
            # Verificando se o cliente ja possui o arquivo completo
            if self.count_file_parts == self.total_file_parts:
                file.close()
                print "O arquivo ja esta completo."
                sys.exit()
            # Requisita as partes do arquivo
            request_parsed = ast.literal_eval(PACKAGE_CLIENT)
            request_parsed["mt"] = MSG_REQUEST
            request_parsed["fn"] = FILE_NAME
            request_parsed["rp"] = self.request_list
            while self.running:
                request_serialized = str(request_parsed)
                # Mantendo o ponteiro sempre no inicio do arquivo
                # Necessario antes de passar o arquivo como parametro pra uma funcao
                file.seek(0)
                if len(self.request_list) == 0:
                    if self.count_file_parts == self.total_file_parts:
                        print "O arquivo foi recebido com sucesso."
                        self.kill()
                    elif self.last_part_requested == (self.total_file_parts - 1):
                        print "O servidor nao possui os pacotes requisitados do arquivo."
                        if not self.connect_to_server(IP_LIST):
                            self.kill()
                        else:
                            self.verify_file_parts(0, file)
                            request_parsed["rp"] = self.request_list
                    else:
                        self.verify_file_parts(self.last_part_requested + 1, file)
                        request_parsed["rp"] = self.request_list
                else:
                    try:
                        self.client_socket.send(request_serialized)
                        serialized_file_part = self.client_socket.recv(BUFFER_SIZE)
                        if serialized_file_part:
                            parsed_file_part = ast.literal_eval(serialized_file_part)
                            if parsed_file_part["mt"] == MSG_REPLY:
                                file_part = parsed_file_part["fp"]
                                # Em erros de comunicacao, o servidor pode enviar o mesmo pacote mais de uma vez
                                # Este if evita o processamento de um pacote replicado
                                if file_part in self.request_list:
                                    file_data = parsed_file_part["fd"]
                                    file.seek(file_part * MESSAGE_SIZE)
                                    file.write(file_data)
                                    self.count_file_parts += 1
                                    self.request_list.remove(file_part)
                                    request_parsed["rp"] = self.request_list
                            # O par nao possui as partes requisitadas do arquivo
                            elif parsed_file_part["mt"] == MSG_PACKAGE_NOT_FOUND:
                                self.request_list = []
                    except socket.error:
                        print "Erro ao tentar estabelecer comunicacao com o servidor."
                        self.num_errors += 1
                    if self.num_errors == MAX_ERRORS:
                        print "Numero maximo de erros atingido."
                        if not self.connect_to_server(IP_LIST):
                            self.kill()
            file.close()
            self.client_socket.close()

    # Prompt, object instantiation, and threads start here.

    ip_addr = raw_input('Digite servidor ou cliente: ')

    if ip_addr.lower() == 'servidor':
        server = Server()
        server.start()
    else:
        client = Client()
        client.start()

if __name__ == "__main__":
    main()